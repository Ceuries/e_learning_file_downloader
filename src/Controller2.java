import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.DomNode;
import com.gargoylesoftware.htmlunit.html.HtmlAnchor;
import com.gargoylesoftware.htmlunit.util.Cookie;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TextField;
import java.io.IOException;
import java.util.List;
import java.util.Set;

public class Controller2 {
    private WebClient webClient;
    private Set<Cookie> cookies;
    private ObservableList<String>courseOption;
    @FXML
    ComboBox<String> courses;
    @FXML
    TextField selectedCourse;
    private List<DomNode> currentCourse;
    @FXML
    TextField downloadFile;
    @FXML
    ProgressIndicator downloadProgress;
    @FXML
    Label downloadProgress2;

    public void initialize(){
    }

    @FXML
    private void allowCourseSelect() throws IOException {
        courseSelection();
    }

    void setCourse (ObservableList<String> options){
        courseOption = options;
        courses.setItems(courseOption);
        courses.getSelectionModel().select(0);
    }

    void setCookies(Set<Cookie> c){
        cookies = c;
    }
    void setWebClient(WebClient w){
        webClient = w;
    }


    void setCurrentCourse(List<DomNode> cs){

        currentCourse = cs;
    }


    private void courseSelection() throws IOException {
        String course = courses.getValue();
        selectedCourse.setText(course);
        //Access the 1st course anchor to allow download file exist for the
        HtmlAnchor courseLinks = currentCourse.get(courseOption.indexOf(course)).querySelector("a");
        //Enter the course link
        LoadProcess2 load = new LoadProcess2(courseLinks,cookies,webClient,downloadFile,downloadProgress,downloadProgress2);
        new Thread(load).start();

    }




}
