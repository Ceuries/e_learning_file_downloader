import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.DomNode;
import com.gargoylesoftware.htmlunit.html.HtmlAnchor;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.util.Cookie;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TextField;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class LoadProcess2 extends Task<HtmlPage> {

    private WebClient webClient;
    private Set<Cookie> cookies;
    private HtmlAnchor coursePage;
    private TextField downloadFile;
    private ProgressIndicator downloadProgress;
    private Label downloadProgress2;

    LoadProcess2(HtmlAnchor a, Set<Cookie> c ,WebClient w,TextField df,ProgressIndicator dp,Label dp2){
        webClient = w;
        coursePage = a;
        cookies = c;
        downloadFile = df;
        downloadProgress = dp;
        downloadProgress2 = dp2;
    }

    @Override
    protected HtmlPage call() throws Exception {
        HtmlPage login = changePage(coursePage);
        HttpClientContext context = setupCookie(cookies);
        CloseableHttpClient httpClient = initializeHttpClient();
        downloadFiles(context,login,httpClient,webClient);
        return null;

    }

    private HtmlPage changePage(HtmlAnchor link) throws IOException {
        return link.click();
    }

    private static HttpClientContext setupCookie(Set<Cookie> cookies){
        Iterator<Cookie> it = cookies.iterator();
        org.apache.http.client.CookieStore cookieStore = new BasicCookieStore();
        while (it.hasNext())
            cookieStore.addCookie(it.next().toHttpClient());
        HttpClientContext context = HttpClientContext.create();
        context.setCookieStore(cookieStore);
        return context;
    }

    private static CloseableHttpClient initializeHttpClient()
    {
        int CONNECTION_TIMEOUT = 80000;
        RequestConfig requestConfig = RequestConfig.custom().setCookieSpec(CookieSpecs.DEFAULT)
                .setConnectionRequestTimeout(CONNECTION_TIMEOUT)
                .setConnectTimeout(CONNECTION_TIMEOUT)
                .setSocketTimeout(CONNECTION_TIMEOUT)
                .build();

        return HttpClients.custom().setDefaultRequestConfig(requestConfig).disableContentCompression().build();
    }

    private void downloadFiles(HttpClientContext context, HtmlPage login, CloseableHttpClient httpclient, WebClient webClient) throws IOException {
        List<DomNode> download = login.querySelectorAll(".activityinstance a");
        for (DomNode aDownload : download) {
            HtmlAnchor link = (HtmlAnchor) aDownload;
            URL url = new URL(link.getAttribute("href"));
            HttpGet httpGet = new HttpGet(url.toString());
            CloseableHttpResponse response = httpclient.execute(httpGet, context);
            HttpEntity entity = response.getEntity();
            InputStream in = entity.getContent();

            if (entity.getContentType().toString().equals("Content-Type: text/html; charset=utf-8")) {

                HtmlPage login2 = webClient.getPage(url);
                EntityUtils.consumeQuietly(response.getEntity());
                if (login2.asXml().contains("resourceworkaround")) {
                    HtmlAnchor link2 = login2.querySelector(".resourceworkaround a");
                    url = new URL(link2.getAttribute("href"));
                    HttpGet httpGet2 = new HttpGet(url.toString());
                    HttpResponse response2 = httpclient.execute(httpGet2, context);
                    HttpEntity entity2 = response2.getEntity();
                    in = entity2.getContent();
                    String urlString = url.toString();
                    String fileName = urlString.substring(urlString.lastIndexOf('/') + 1, urlString.length());
                    //Path where the file will be downloaded
                    String file = "C:\\" + fileName;

                    if (fileName.contains(".pdf") || fileName.contains(".docx") || fileName.contains(".pptx")) {
                        //Execute download file method
                        saveFile(in, file, fileName, entity2.getContentLength());
                    } else
                        EntityUtils.consumeQuietly(response2.getEntity());
                }

            } else {
                String urlString;
                //get the URL of the download link
                urlString = context.getRedirectLocations().get(0).toString();
                //Get the filename of the file
                String fileName = urlString.substring(urlString.lastIndexOf('/') + 1, urlString.length()).replace("%20"," ");

                //Path where the file will be downloaded
                String file = "C:\\" + fileName;
                if (fileName.contains(".pdf") || fileName.contains(".docx") || fileName.contains(".pptx")) {
                    //Execute download file method
                    saveFile(in, file, fileName, entity.getContentLength());
                } else
                    EntityUtils.consumeQuietly(response.getEntity());
            }
        }
    }
    
    private void saveFile(InputStream in, String file, String filename, long completeFileSize) throws IOException {
        Platform.runLater(() -> {

            downloadFile.setText(filename);
            downloadProgress.setProgress(0);
            downloadProgress2.setText("0%");
        });
        System.out.println("opening connection");

        FileOutputStream fos = new FileOutputStream(new File(file));
        long currentTime = System.currentTimeMillis();
        System.out.println("reading file...");
        int length;
        byte[] buffer = new byte[1024];// buffer for portion of data from
        // connection
        long downloadedFileSize = 0;
        while ((length = in.read(buffer)) > -1) {
            downloadedFileSize += length;
            final int currentProgress = (int) (((double)downloadedFileSize) / ((double)completeFileSize)*100d);
            long timeElapsed = System.currentTimeMillis() - currentTime;
            if( timeElapsed >= 2000 || (currentProgress == 100) ) {
                System.out.println("Downlaoded: " + currentProgress + "%");
                Platform.runLater(() -> {
                    downloadProgress.setProgress(((float)currentProgress/100));
                    downloadProgress2.setText(currentProgress+"%");
                });
                currentTime = System.currentTimeMillis();
            }

            fos.write(buffer, 0, length);

        }
        fos.close();
        in.close();
        System.out.println(filename + " is downloaded");
        System.out.println("It is located at C:\\"+ filename);
    }
}
