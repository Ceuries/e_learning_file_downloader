import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.*;
import com.gargoylesoftware.htmlunit.util.Cookie;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.ExecutionException;


public class Controller {

    private HtmlPage login;

    private WebClient webClient;
    @FXML
    PasswordField loginpassword;
    @FXML
    TextField loginemail;
    @FXML
    ProgressIndicator progress = new ProgressIndicator();
    @FXML
    Button submit;


    public void initialize() throws IOException, InterruptedException {
        webClient = initializeWebClient();
        progress.setVisible(false);

    }


    @FXML
    public void allowLogin() throws IOException, InterruptedException, ExecutionException {

        submit.setVisible(false);
        progress.setProgress(-1.0f);
        progress.setVisible(true);
        Stage stage = (Stage) submit.getScene().getWindow();
    LoadProcess load = new LoadProcess(webClient,loginemail,loginpassword);
    new Thread(load).start();
        load.setOnSucceeded(event -> {
            login = load.getValue();
            progress.setProgress(1.0f);

            try {
                Set<Cookie> cookies = webClient.getCookieManager().getCookies();
                setCourse(login,stage,cookies);
            } catch (IOException e) {
                e.printStackTrace();
            }


        });


    }

    private static WebClient initializeWebClient(){
        //Uses Firefox for the webClient Simulation
        WebClient webClient = new WebClient(BrowserVersion.CHROME);

        webClient.getOptions().setThrowExceptionOnScriptError(false);
        //Enable Redirection
        webClient.getOptions().setRedirectEnabled(true);
        //Enable Cookie
        webClient.getCookieManager().setCookiesEnabled(true);
        //Enable Javascript
        webClient.getOptions().setJavaScriptEnabled(true);
        //Disable CSS
        webClient.getOptions().setCssEnabled(false);
        //Enable Use of Insecure SSL
        webClient.getOptions().setUseInsecureSSL(true);
        //Turn off Logging for better view of output
        java.util.logging.Logger.getLogger("com.gargoylesoftware").setLevel(java.util.logging.Level.OFF);

        return webClient;
    }

    private void setCourse(HtmlPage login, Stage stage,Set<Cookie> cookies) throws IOException {
        //Access the courses and print out the courses that are taken on that semester
        List<DomNode> currentCourse = login.querySelector(".unlist").querySelectorAll("li");
        List<String> options = new ArrayList<>();
        for (DomNode aCurrentCourse : currentCourse) {
            options.add(aCurrentCourse.getTextContent());
        }
        ObservableList<String> addOptions = FXCollections.observableArrayList(options);

        FXMLLoader loader =new FXMLLoader(
                getClass().getResource(
                        "download.fxml"
                )
        );

        stage.setScene(new Scene(loader.load()));
        Controller2 controller = loader.getController();
        controller.setCourse(addOptions);
        controller.setCurrentCourse(currentCourse);
        controller.setCookies(cookies);
        controller.setWebClient(webClient);
        stage.show();


    }
}

