import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.*;
import javafx.concurrent.Task;
import javafx.scene.control.*;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.List;
public class LoadProcess extends Task<HtmlPage> {

    private WebClient webClient;
    private TextField loginemail;
    private PasswordField loginpassword;


    LoadProcess(WebClient wc, javafx.scene.control.TextField le, PasswordField lp){
        webClient = wc;
        loginemail = le;
        loginpassword = lp;
    }

    @Override
    protected HtmlPage call() throws Exception {

        return loginAuthentication(webClient);

    }


    private  HtmlPage loginAuthentication(WebClient webClient) throws IOException, InterruptedException {
        //save the page of e-learning into Html page to be able to access the page
        HtmlPage page = webClient.getPage("http://elearning.usm.my/sidang1617/");
        //Access the login button's anchor and store into links
        List<HtmlAnchor> links = page.getByXPath("//div[@class='content']//a");
        //Click the anchor links and proceed to the login page
        HtmlPage login = links.get(0).click();
        //Fill up the login form and login into the e-learning page
        final HtmlForm form = login.getFormByName("aspnetForm");
        final HtmlTextInput username = form.getInputByName("ctl00$ContentPlaceHolder1$UsernameTextBox");
        String email = loginemail.getText();
        username.setValueAttribute(email);
        final HtmlPasswordInput password = form.getInputByName("ctl00$ContentPlaceHolder1$PasswordTextBox");
        String pass = loginpassword.getText();
        password.setValueAttribute(pass);
        HtmlInput button = form.getInputByValue("Sign In");
        login = button.click(); //Submit the form

        //Wait for login completion so that the website is fully loaded to be able to access it.
        while (login.asText().equals("Working...") || !login.asText().contains("Dashboard") || login.asText().contains("Loading...")) {
            synchronized (login) {
                login.wait(1000);
            }
            login = (HtmlPage) webClient.getCurrentWindow().getEnclosedPage();
        }

        return login;
    }



}
